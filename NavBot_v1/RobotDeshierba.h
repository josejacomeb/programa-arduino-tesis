#include <Servo.h> 
#include<math.h>
// Variables mototres y encoders
#define ENCODERizquierda 3
#define ENCODERderecha 2
#define DIRMOTORDCDer 4
#define VELMotorDCDer 5
#define DIRMOTORDCIzq 7
#define VELMotorDCIzq 6
#define MOTORHerramienta 8
#define SERVO1 10
#define SERVO2 11
#define SERVO3 9
#define Paro 12
#define Piloto 13
// VARIABLEAS PARA ROBOT DELTA
//*********************************************
Servo myservo, myservo1, myservo2;
int var1;
double L1=176.0, L2=500.0, r=96.0, rpm=80.0,R=16.0;
float Px=0.0,Py=0.0,Pz=-340.0,L,beta,alfamin,la2,lb2,r2,b2,k;
float pi = 3.141592653;
float alfamax=pi/2;
//double ang=pi/3;
float fi[3] = {pi/2, 7*pi/6, -pi/6};
//float fi[3] = {0, 2*pi/3, -2*pi/3};
 double ct[3]={cos(fi[0]),cos(fi[1]),cos(fi[2])};
 double st[3]={sin(fi[0]),sin(fi[1]),sin(fi[2])};
double tetha[3]={0,0,0};
double serv[3]={0,0,0};
double x[3]={0,0,0};
double y[3]={0,0,0};
double z[3]={0,0,0};
double q[3]={0,0,0};
double s[3]={0,0,0};
double b1[3]={0,0,0};
double b3[3]={0,0,0};
double b4[3]={0,0,0};
double b5[3]={0,0,0};
double b6[3]={0,0,0};
double alfat[3]={0,0,0};
double aa[3]={0,0,0};
double aan[3]={0,0,0};
float a1=0.0,a3=0.0;
int a2,a4,pwm1,pwm2;
float acum1=120.0,acum2=120.0,acum3=120.0,div1,div2,div3;
float ang1=91.00,ang2=86.00,ang3=85.00;
char opcion = 'o';

 //**********************************************************
//VARIABLES PARA GUARDAR EL VECTOR
String m;
float puntos;
char id;
//*************************************************************

bool paro = false, paroexterno = false;
bool anteriorderecha = false, anteriorizquierda = false;
int contadorderecha = 0, contadorizquierda = 0;
int contadorparcial = 30;
const int pwmderecha = 28, pwmizquierda = 30;
const int pulsosmax = 90;

// The following defines are used by the main sketch
// 
// Navigator defines
#define WHEELBASE               nvMM(600)     // millimeters
#define WHEEL_DIAMETER          nvMM(250)     // millimeters
#define TICKS_PER_REV           20

#define BUTTON_PIN              12

// correct for systematic errors
#define WHEEL_RL_SCALER         1.0f  // Ed
#define WHEELBASE_SCALER        1.0f  // Eb

// correct distance 
#define DISTANCE_SCALER         1.0f  // Es


//----------------------------------------
// Motor config
//----------------------------------------

// use these to correct for incorrect motor wiring
#define SWAP_MOTORS             0
#define RMOTOR_DIR              1L    // -1L to reverse, 1L for normal
#define LMOTOR_DIR              1L    // -1L to reverse, 1L for normal


//----------------------------------------
//
//----------------------------------------
void llantaizquierda(int16_t velocidad){
  if(velocidad > 0){
      digitalWrite(DIRMOTORDCIzq,HIGH); //Ir Para adelante
      analogWrite(VELMotorDCIzq,velocidad);
  }
  else if(velocidad == 0){
  analogWrite(VELMotorDCIzq,0);     
 }  
 else{
      digitalWrite(DIRMOTORDCIzq,LOW); //Ir Para adelante
      analogWrite(VELMotorDCIzq,-velocidad);
 }
}
void llantaderecha(int16_t velocidad){
  if(velocidad > 0){
      digitalWrite(DIRMOTORDCDer,HIGH); //Ir Para adelante
      analogWrite(VELMotorDCDer,velocidad);
  }
  else if(velocidad == 0){
  analogWrite(VELMotorDCDer,0);     
 }  
 else{
      digitalWrite(DIRMOTORDCDer,LOW); //Ir Para adelante
      analogWrite(VELMotorDCDer,-velocidad);  
 }
}
volatile int16_t contarizquierda(){
  if(digitalRead(ENCODERizquierda) != anteriorizquierda){
      contadorizquierda += 1;
    }
    anteriorizquierda = digitalRead(ENCODERizquierda);
    return contadorizquierda;
}
volatile int16_t contarderecha(){
    if(digitalRead(ENCODERderecha) != anteriorderecha){
      contadorderecha += 1;
    }
    anteriorderecha = digitalRead(ENCODERderecha);
    return contadorderecha;
}


void init_bot()
{
  pinMode(Paro, INPUT);
  pinMode(ENCODERizquierda, INPUT);
  pinMode(ENCODERderecha, INPUT);
  pinMode(DIRMOTORDCDer, OUTPUT);
  pinMode(VELMotorDCDer, OUTPUT);
  pinMode(DIRMOTORDCIzq, OUTPUT);
  pinMode(VELMotorDCIzq, OUTPUT);
  pinMode(MOTORHerramienta, OUTPUT);
  myservo.attach(SERVO1);
  myservo1.attach(SERVO2);
  myservo2.attach(SERVO3);
  pinMode(Piloto, OUTPUT);
  anteriorderecha = digitalRead(ENCODERderecha);
  anteriorizquierda = digitalRead(ENCODERizquierda);
  myservo.write(88);
  myservo1.write(93);
  myservo2.write(93);
  digitalWrite(Piloto, HIGH);
  digitalWrite(MOTORHerramienta, HIGH);
  Serial.begin(115200);
  // calculos para Cinematica inv 
 //*************************************
 L=sqrt(pow(L2,2)+pow(L1,2)-2*L1*L2);
 beta=asin(R/L);
 alfamin=-((pi/2)-beta);
 lb2=pow(L2,2);
 la2=pow(L1,2);
 r2=pow(r,2);
 b2=4*r2;
 //**************************************
  // do all bot initialization here

  // you can also access the pilot and navigator if
  // want to do furtner inititalization that's
  // specific to your bot

  // e.g.
  //pilot.SetMinServiceInterval( nvMS(20));

}

//----------------------------------------
// Motor handler
//----------------------------------------

void motor_handler( Pilot *pilot, int16_t lmotor, int16_t rmotor)
{
 
    // convert lmotor and rmotor to your motor controller's range 
  int16_t lspeed = ((lmotor*7   0L)/1024L)*LMOTOR_DIR;  //Convierto hasta 24 
  int16_t rspeed = ((rmotor*70L)/1024L)*RMOTOR_DIR;
  
  // put your motor code in here  
  #if SWAP_MOTORS
    llantaderecha(rspeed);
    llantaizquierda(lspeed);
  #else
    llantaderecha( lspeed );
    llantaizquierda( rspeed );
  #endif

  #if MOTOR_INFO || TEST_MOTORS
  Serial.print(F("Motors: Left = "));
  Serial.print(lspeed);
  Serial.print(F(" ("));
  Serial.print(lmotor);
  Serial.print(F(")"));
  Serial.print(F(" - Right = "));
  Serial.print(rspeed);
  Serial.print(F(" ("));
  Serial.print(rmotor);
  Serial.println(F(")"));
  #endif
 
}

//----------------------------------------
// Ticks handler
//----------------------------------------
 
bool ticks_handler( Pilot *pilot, int16_t *lft, int16_t *rht)
{
  // read the encoders and set lft and rht
   *lft = contarizquierda();
    *rht = contarderecha();
    contadorizquierda = 0;
    contadorderecha = 0;

  // return true if successful or false if there was an error
  return true;
}



